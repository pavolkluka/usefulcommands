# UsefulCommands

All commands I use in my scripts when they are needed.

## GREP

Log example:
```json
Aug 17 13:52:04 10.164.1.70 1 2016-08-17T11:52:04.780Z eras ERAServer 12068 Syslog
{"event_type":"Threat_Event","ipv4":"11.163.217.148","source_uuid":"37b1bc9a-4792-451b-a673-b56f36c8f81c","occured":"17-Aug-2016
10:33:58","severity":"Critical","threat_type":"application","threat_name":"Win32/Adware.SpeedingUpMyPC.AM","threat_flags":"Variant","scanner_id":"On-demand
scanner","scan_id":"ndl24602.dat","engine_version":"13974 (20160817)","object_type":"file","object_uri":"file:///C:/Documents and
Settings/uzivatel.DOMENA/Desktop/SW/2 SW/RU KIS Zapad/Hiren's.BootCD.15.2.iso/HBCD\\Programs\\Files\\DeviceDoctor.7z","threat_handled":false,"need_restart":false,
"username":"DOMENA\\uzivatel"}
Aug 25 11:39:54 10.164.1.70 1 2016-08-25T09:39:54.596Z eras ERAServer 12068 Syslog
{"event_type":"FirewallAggregated_Event","ipv4":"173.16.10.48","source_uuid":"c3dc0dc8-592d-485f-bc3d-a385e94d1188","occured":"25-Aug-2016
09:11:51","severity":"Critical","event":"Detected attack against security
hole","source_address":"193.167.56.1","source_address_type":"IPv4","source_port":49422,"target_address":"193.167.56.1","target_address_type":"IPv4","target_port":4
45,"protocol":"TCP","account":"DOMENA\\uzivatel","process_name":"C:\\Program Files\\Oracle\\VirtualBox
\\VirtualBox.exe","rule_name":"Win32/Exploit.SMB.CVE-2009-3103","inbound":false,"aggregate_count":1}
```

When I need extract IP addresses (fields **ipv4** and **address**) from log in JSON format (in this example) follow this command:
```bash
grep -oP "(?<=ipv4\"\:\"|address\"\:\")[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(?=\")" threats.log
```

Output:
```bash
11.163.217.148
173.16.10.48
193.167.56.1
193.167.56.1
```
For extracting **username** or **account** use this command:
```bash
grep -oP "(?<=username\"\:\"|account\"\:\").*?(?=\")" threats.log
```

Output:
```bash
DOMENA\\uzivatel
DOMENA\\uzivatel
```

OK, so what if I have XML file (for this example I use Windows Task xml file) type and I need extract some values from XML tags.

File example:
```xml
<?xml version="1.0" encoding="UTF-16"?>
<Task version="1.2" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
<RegistrationInfo>
<Author>Adobe Systems Incorporated</Author>
<Description>This task keeps your Adobe Flash Player installation up to date with the latest enhancements and security fixes. If this task is disabled or
removed, Adobe Flash Player will be unable to automatically secure your machine with the latest security fixes.</Description>
</RegistrationInfo>
<Triggers>
<CalendarTrigger id="UpdateTrigger">
<Repetition>
<Interval>PT1H</Interval>
<Duration>P1D</Duration>
<StopAtDurationEnd>false</StopAtDurationEnd>
</Repetition>
<StartBoundary>2000-01-01T00:00:00Z</StartBoundary>
<Enabled>true</Enabled>
<ScheduleByDay>
<DaysInterval>1</DaysInterval>
</ScheduleByDay>
</CalendarTrigger>
</Triggers>
<Settings>
<MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
<DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>
<StopIfGoingOnBatteries>true</StopIfGoingOnBatteries>
<AllowHardTerminate>true</AllowHardTerminate>
<StartWhenAvailable>true</StartWhenAvailable>
<RunOnlyIfNetworkAvailable>true</RunOnlyIfNetworkAvailable>
<IdleSettings>
<Duration>PT10M</Duration>
<WaitTimeout>PT1H</WaitTimeout>
<StopOnIdleEnd>true</StopOnIdleEnd>
<RestartOnIdle>false</RestartOnIdle>
</IdleSettings>
<AllowStartOnDemand>true</AllowStartOnDemand>
<Enabled>true</Enabled>
<Hidden>false</Hidden>
<RunOnlyIfIdle>false</RunOnlyIfIdle>
<WakeToRun>false</WakeToRun>
<ExecutionTimeLimit>PT72H</ExecutionTimeLimit>
<Priority>7</Priority>
</Settings>
<Actions Context="Author">
<Exec>
<Command>C:\Windows\SysWOW64\Macromed\Flash\FlashPlayerUpdateService.exe</Command>
</Exec>
</Actions>
<Principals>
<Principal id="Author">
<UserId>SYSTEM</UserId>
<LogonType>InteractiveToken</LogonType>
<RunLevel>LeastPrivilege</RunLevel>
</Principal>
</Principals>
LINUX Strana 1</Principals>
</Task>
```

For extracting value from **Command** follow this command:
```bash
grep -oP '(?<=<Command>).*?(?=</Command>)' task.xml
```
Output:
```bash
C:\Windows\SysWOW64\Macromed\Flash\FlashPlayerUpdateService.exe
```

## AWK

Next situation.
I have file with domains list (for example malicious domain) and I want to get domains from file and save it to another file with own descriptions.

Content of file with domains list:
```bash
Site
abcdefg724.com
poamsne.com
mc.mcseeen.top
tr1688888.gicp.net
siasdwjj.s.f5555.org
...
```
Now **AWK** is my friend, check this command:
```bash
awk -v domain_desc="Suspicious Domain" '/^[0-9a-zA-Z\.\-]+\.[a-z]{2,10}/ { print $1 ",Domain description:" domain_desc "." }' domain.txt
```
Output:
```bash
abcdefg724.com,Domain description:Suspicious Domain
poamsne.com,Domain description:Suspicious Domain
mc.mcseeen.top,Domain description:Suspicious Domain
tr1688888.gicp.net,Domain description:Suspicious Domain
siasdwjj.s.f5555.org,Domain description:Suspicious Domain
...
```
Output file is in CSV format with comma delimiter.

So now I have a file with IP addresses and want to do same think.
```bash
awk -v ip_desc="Suspicious IP Address" '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ { print $1 ",IP Address description:" ip_desc "." }' ip.txt
```
Output:
```bash
101.109.246.16,IP Address description:Suspicious IP Address.
101.205.120.43,IP Address description:Suspicious IP Address.
101.228.208.34,IP Address description:Suspicious IP Address.
101.230.200.173,IP Address description:Suspicious IP Address.
101.231.101.140,IP Address description:Suspicious IP Address.
...
```
If I work with a file that has a character (for this example is unwanted **space**) and I want to exclude it.

Input file example:
```bash
78.146.219.54 # Bad Host
78.22.246.22 # Bad Host
86.15.238.210 # Bad Host;Scanning Host
76.53.39.244 # Suspicious Host;Spamming
81.65.169.157 # Suspicious Host
72.111.244.164 # Malicious Host
```
Execute this command:
```bash
awk -F '#' '/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/ { gsub(" ","",$1); gsub(" ","",$2); print $1 ",IP Blocklist (" $2 ")" }' iplist.txt
```
Output:
```bash
78.146.219.54,IP Blocklist (Bad Host)
78.22.246.22,IP Blocklist (Bad Host)
86.15.238.210,IP Blocklist (Bad Host;Scanning Host)
76.53.39.244,IP Blocklist (Suspicious Host;Spamming)
81.65.169.157,IP Blocklist (Suspicious Host)
72.111.244.164,IP Blocklist (Malicious Host)
```

